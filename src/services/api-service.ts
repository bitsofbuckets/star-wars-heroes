export class ApiService {

    private baseUrl = 'https://swapi.dev/api/';

    async getHero(queryString: string): Promise<any> {
        const path = this.baseUrl + '/people/?search=' + queryString;

        return await fetch(path)
        .then(response => response.json())
        .then((data: any) => {
            return Promise.resolve(data.results);
        })
        .catch((err: any) => {
            console.error('');
            // do something
            return Promise.reject('Oops, something has gone wrong. Please try again.');
        });

    }

}