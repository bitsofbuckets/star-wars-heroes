import { render } from '@testing-library/react';
import { shallow } from 'enzyme';
import App from './App';


describe('<App />', () => {

    test('renders the search form', () => {
        const { getByText } = render(<App/>);
        expect(getByText(/search/i)).toBeInTheDocument();
    });

    describe('API', () => {

        let app: any;

        beforeEach(() => {
            // shallow rendering
            // not asserting on behaviour of child components
            app = shallow<App>(<App />);
        });

        afterEach(() => {
            app.unmount();
         });

        test('should present loading spinner on search', () => {
            // call submit
            app.instance().formSubmit('vader');
            expect(app.exists('.loader')).toEqual(true);
        });

        test('should dismiss loading spinner after ajax', async () => {

            const api = app.instance().apiService;

            // api spy
            const getHero = jest.spyOn(api, 'getHero').mockImplementationOnce(() => {
                return Promise.resolve([]); // empty array, that's fine
            });

            await app.instance().formSubmit('vader');
            expect(app.exists('.loader')).toEqual(false);
        });
    
        test('gets data on submit form', async () => {

            const api = app.instance().apiService;

            // api spy
            const getHero = jest.spyOn(api, 'getHero').mockImplementationOnce(() => {
                return Promise.resolve([]); // empty array, that's fine
            });

            // call submit on form
            await app.instance().formSubmit('vader');

            expect(getHero).toHaveBeenCalledWith('vader');

        });

        test('gets and sets data on submit form', async () => {

            const mockHero: any = {
                name: 'Vader Man',
                gender: 'male',
                birth_year: '1984',
                height: 'many tall',
                mass: 'lots',
                hair_color: 'singed'
            };

            const api = app.instance().apiService;

            // api spy
            const getHero = jest.spyOn(api, 'getHero').mockImplementationOnce(() => {
                return Promise.resolve([mockHero]);
            });

            // call submit on form
            await app.instance().formSubmit('vader');

            // gets
            expect(getHero).toHaveBeenCalledWith('vader');
            // sets
            expect(app.instance().state.heroes[0].name).toEqual(mockHero.name);

        });

        test('shows empty state on no search results', async () => {

            const api = app.instance().apiService;

            // api spy
            const getHero = jest.spyOn(api, 'getHero').mockImplementationOnce(() => {
                return Promise.resolve([]); // empty array from api
            });

            // call submit on form
            await app.instance().formSubmit('asdf');

            // gets
            expect(getHero).toHaveBeenCalledWith('asdf');
            // sets
            expect(app.instance().state.error).toEqual('No results');

        });

        test('handles api error and shows error message', async () => {
            const api = app.instance().apiService;

            // api spy
            const getHero = jest.spyOn(api, 'getHero').mockImplementationOnce(() => {
                return Promise.reject('Oh my!');
            });

            // call submit on form
            await app.instance().formSubmit('vader');

            expect(app.instance().state.error).toEqual('Oh my!');
            
        });

    });

});

