import { render, screen, fireEvent, act } from '@testing-library/react';
import SearchForm from './SearchForm';


describe('<SearchForm />', () => {

    test('should validate search input and provides error message', async () => {

        const onSubmit = jest.fn();
        const { getByText, getByDisplayValue } = render(<SearchForm onSubmit={onSubmit} />);
        const submitButton = getByDisplayValue("SEARCH");
        
        await act (async () => {
            fireEvent.change(screen.getByLabelText(/name/i), {
                target: {value: ''}, // no user input
            });
        });

        await act (async () => {
            fireEvent.submit(submitButton);
        });

        expect(getByText("Please enter something..")).toBeInTheDocument();

    });

});


