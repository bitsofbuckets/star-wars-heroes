import React from 'react';
import styles from './SearchForm.module.css'; 

class SearchForm extends React.Component<SubmitForm, FormState> {

    state: FormState = {
        name: '',
        formError: 'Please enter something..',
        formValid: true
    };

    constructor(props: SubmitForm) {
        super(props);

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(event: React.FormEvent<HTMLInputElement>): void {
        this.setState({name: event.currentTarget.value}, () => {
            this.validateForm();
        });
    }

    handleSubmit(event: React.SyntheticEvent): void {

        event.preventDefault();

        this.validateForm(() => {
            if (!this.state.formValid) return;
            this.props.onSubmit(this.state.name);
        });

    }

    validateForm(callBack?: () => void): void {
        const valid = this.state.name.length ? true : false;
        this.setState({ formValid: valid }, callBack);
    }

    render() {

        const formError = () => {
            if (!this.state.formValid) {
                return (
                    <div className={styles.errorText}>
                        {this.state.formError}
                    </div>
                )
            }
        };

        return (
            <div className={styles.container}>
                <form onSubmit={this.handleSubmit}>
                    <label>Character Name</label>

                    {formError()}

                    <div className={styles.flexGrid}>
                        <div className={styles.col}>
                            <input type="text" value={this.state.name} name="name" aria-label="name" onChange={this.handleChange} placeholder="darth.." />
                        </div>
                        <div className={styles.col}>
                            <input className={styles.button} type="submit" value="SEARCH" name="search" />
                        </div>
                    </div>
                </form>
            </div>
        );
    }

}

type FormState = {
    name: string;
    formError: string;
    formValid: boolean;
}

interface SubmitForm {
    onSubmit: (name: string) => void;
}

export default SearchForm;