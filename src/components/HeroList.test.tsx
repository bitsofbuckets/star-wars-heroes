import HeroList, { Hero } from './HeroList';
import { shallow } from 'enzyme';


describe('<HeroList />', () => {

    test('should show five rows of data', () => {

        const mockHero: Hero = {
            name: 'Vader Man',
            gender: 'male',
            birthYear: '1984',
            height: 'many tall',
            mass: 'lots',
            hairColor: 'singed'
        };

        const heroList = shallow<HeroList>(<HeroList hero={mockHero}/>);

        expect(heroList.find('.row')).toHaveLength(5);

    });

});


