import React from 'react';
import styles from './HeroList.module.css'; 

class HeroList extends React.Component<HeroProps> {

    render() {
        return (
            <div>
                <div className={styles.heroContainer}>
                    <div className={styles.header}>
                        {this.props.hero.name}
                    </div>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            Gender
                        </div>
                        <div className={styles.col}>
                            {this.props.hero.gender}
                        </div>
                    </div>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            Birth year
                        </div>
                        <div className={styles.col}>
                            {this.props.hero.birthYear}
                        </div>
                    </div>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            Height
                        </div>
                        <div className={styles.col}>
                            {this.props.hero.height}
                        </div>
                    </div>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            Mass
                        </div>
                        <div className={styles.col}>
                            {this.props.hero.mass}
                        </div>
                    
                    </div>
                    <div className={styles.row}>
                        <div className={styles.col}>
                            Hair Color
                        </div>
                        <div className={styles.col}>
                            {this.props.hero.hairColor}
                        </div>
                    </div>
                </div>

            </div>
        );
    }

}

export type Hero = {
    name: string;
    gender: string;
    birthYear: string;
    height: string;
    mass: string;
    hairColor: string;
};

interface HeroProps {
    hero: Hero
}

export default HeroList;