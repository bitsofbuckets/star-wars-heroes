import React from 'react';

import './App.css';

import HeroList, { Hero } from './components/HeroList';
import SearchForm from './components/SearchForm';

import { ApiService } from './services/api-service';

class App extends React.Component {

    apiService: ApiService;

    state = {
        heroes: [],
        inAjax: false,
        error: ''
    }

    constructor(props: any) {
        super(props);
        this.formSubmit = this.formSubmit.bind(this);
        this.apiService = new ApiService();
    }

    async formSubmit(searchQuery: string): Promise<any> {

        const noHeroesError = 'No results';

        this.setState({
            inAjax: true,
            heroes: [],
            error: ''
        });

        await this.apiService.getHero(searchQuery)
        .then((res: Array<any>) => {

            const heroes: Array<Hero> = [];

            res.forEach((item: any) => {

                const hero: Hero = {
                    name: item.name,
                    gender: item.gender,
                    birthYear: item.birth_year,
                    height: item.height,
                    mass: item.mass,
                    hairColor: item.hair_color
                }

                heroes.unshift(hero);

            });

            this.setState({
                heroes: heroes,
                inAjax: false,
                error: heroes.length ? '' : noHeroesError
            });

        })
        .catch((err: string) => {
            this.setState({inAjax: false, error: err});
        });
    }

    render () {

        const loader = () => {
            if (this.state.inAjax) {
                return (
                    <div className="loader"></div>
                );
            }
        };

        const error = () => {
            if (this.state.error) {
                return (
                    <div className="error">
                        {this.state.error}
                    </div>
                )
            }
        }

        return (
            <div className="main-container">
                <div className="card">
                    <div className="card-header">
                        STAR WARS HEROES
                    </div>
                    <div className="card-content">

                        <SearchForm onSubmit={this.formSubmit} />

                        {
                            this.state.heroes.map((hero, index) => (
                                <HeroList key={index} hero={hero} />
                            ))
                        }

                        <div className="loading-container">
                            {loader()}
                        </div>

                        <div className="error-container">
                            {error()}
                        </div>

                    </div>
                </div>
            </div>
        );
    }

}

export default App;
